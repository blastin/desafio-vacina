package br.com.projeto.adaptador.vacina;

import br.com.projeto.aplicacao.vacina.CasoDeUsoCadastrarVacina;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vacina")
final class ControladorVacina {

    ControladorVacina(final CasoDeUsoCadastrarVacina casoDeUsoCadastrarVacina) {
        this.casoDeUsoCadastrarVacina = casoDeUsoCadastrarVacina;
    }

    private final CasoDeUsoCadastrarVacina casoDeUsoCadastrarVacina;

    @PostMapping
    public ResponseEntity<VacinaResposta> cadastrar(@RequestBody @Validated final VacinaRequisicao vacinaRequisicao) {
        return vacinaRequisicao.cadastrar(casoDeUsoCadastrarVacina).toResponse();
    }

}
