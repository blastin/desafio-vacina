package br.com.projeto.adaptador;

import br.com.projeto.dominio.usuario.UsuarioException;
import br.com.projeto.dominio.vacina.VacinaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class Advice {

    private static final Logger LOGGER = LoggerFactory.getLogger(Advice.class);
    
    @SuppressWarnings("unused")
    private static class ResponseError {

        private ResponseError(final String message, final HttpStatus httpStatus) {
            this.message = message;
            this.httpStatus = httpStatus;
        }

        private final String message;

        private final HttpStatus httpStatus;

        public String getMensagem() {
            return message;
        }

        public int getCodigo() {
            return httpStatus.value();
        }

    }

    @ExceptionHandler(UsuarioException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseError emailOuCpfEncontrado(final UsuarioException usuarioException) {
        LOGGER.error("", usuarioException);
        return new ResponseError(usuarioException.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(VacinaException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseError usuarioComEmailNaoEncontrado(final VacinaException ex) {
        LOGGER.error("", ex);
        return new ResponseError(ex.getMessage(), HttpStatus.CONFLICT);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<ResponseError> emailOuCpfEncontrado(final MethodArgumentNotValidException ex) {
        LOGGER.error("", ex);
        return ex.getAllErrors()
                .stream()
                .map(objectError -> new ResponseError(objectError.getDefaultMessage(), HttpStatus.BAD_REQUEST))
                .collect(Collectors.toUnmodifiableList());
    }
}
