package br.com.projeto.adaptador.vacina;

import br.com.projeto.aplicacao.vacina.CasoDeUsoCadastrarVacina;
import br.com.projeto.dominio.usuario.UsuarioQuery;
import br.com.projeto.dominio.vacina.VacinaCommand;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class VacinaBean {

    @Bean
    VacinaCommand vacinaGateway(final VacinaRepositorio vacinaRepositorio) {
        return vacina -> new VacinalEntity(vacina).salvar(vacinaRepositorio);
    }

    @Bean
    CasoDeUsoCadastrarVacina casoDeUsoCadastrarVacina(final UsuarioQuery usuarioQuery, final VacinaCommand vacinaCommand) {
        return vacinaRequest -> vacinaRequest.cadastrar(usuarioQuery, vacinaCommand);
    }

}
