package br.com.projeto.adaptador.usuario;

import br.com.projeto.aplicacao.usuario.CasoDeUsoCadastrarUsuario;
import br.com.projeto.dominio.Conditional;
import br.com.projeto.dominio.usuario.Usuario;
import br.com.projeto.dominio.usuario.UsuarioElement;
import br.com.projeto.dominio.usuario.UsuarioGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class UsuarioBean {

    @Bean
    UsuarioGateway usuarioGateway(final UsuarioRepositorio repositorio) {
        return new UsuarioGateway() {
            @Override
            public Usuario cadastrar(final UsuarioElement usuario) {
                return new UsuarioEntity(usuario).salvar(repositorio);
            }

            @Override
            public Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf) {
                return Conditional.from(repositorio.existsByCpfOrEmail(cpf, email));
            }

            @Override
            public boolean existeUsuarioPorEmail(final String email) {
                return repositorio.existsByEmail(email);
            }
        };
    }

    @Bean
    CasoDeUsoCadastrarUsuario casoDeUsoCadastrarUsuario(final UsuarioGateway usuarioGateway) {
        return usuarioRequest -> usuarioRequest.cadastrar(usuarioGateway);
    }

}
