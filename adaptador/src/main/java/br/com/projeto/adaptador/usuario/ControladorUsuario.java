package br.com.projeto.adaptador.usuario;

import br.com.projeto.aplicacao.usuario.CasoDeUsoCadastrarUsuario;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
final class ControladorUsuario {

    ControladorUsuario(final CasoDeUsoCadastrarUsuario casoDeUsoCadastrarUsuario) {
        this.casoDeUsoCadastrarUsuario = casoDeUsoCadastrarUsuario;
    }

    private final CasoDeUsoCadastrarUsuario casoDeUsoCadastrarUsuario;

    @PostMapping
    public ResponseEntity<UsuarioResposta> cadastrar(@RequestBody @Validated final UsuarioRequisicao usuarioRequisicao) {
        return usuarioRequisicao.cadastrar(casoDeUsoCadastrarUsuario).toResponse();
    }

}
