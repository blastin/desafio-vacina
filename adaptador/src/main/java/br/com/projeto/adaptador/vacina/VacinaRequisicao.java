package br.com.projeto.adaptador.vacina;

import br.com.projeto.aplicacao.vacina.CasoDeUsoCadastrarVacina;
import br.com.projeto.aplicacao.vacina.VacinaRequest;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@SuppressWarnings("unused")
final class VacinaRequisicao implements VacinaRequest {

    @NotNull
    private String nome;

    public void setNome(final String nome) {
        this.nome = nome;
    }

    @Email
    private String email;

    public void setEmail(final String email) {
        this.email = email;
    }

    @NotNull
    private LocalDate dataRealizacao;

    public void setDataRealizacao(final LocalDate dataRealizacao) {
        this.dataRealizacao = dataRealizacao;
    }

    VacinaResposta cadastrar(final CasoDeUsoCadastrarVacina casoDeUsoCadastrarVacina) {
        return new VacinaResposta(casoDeUsoCadastrarVacina.cadastrar(this));
    }

    @Override
    public String nome() {
        return nome;
    }

    @Override
    public String email() {
        return email;
    }

    @Override
    public LocalDate dataRealizacao() {
        return dataRealizacao;
    }

}
