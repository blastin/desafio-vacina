package br.com.projeto.adaptador.vacina;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

final class VacinaResposta {

    VacinaResposta(final long identificador) {
        this.identificador = identificador;
    }

    private final long identificador;

    @SuppressWarnings("unused")
    public long getId() {
        return identificador;
    }

    @JsonIgnore
    ResponseEntity<VacinaResposta> toResponse() {
        return ResponseEntity.status(HttpStatus.CREATED).body(this);
    }

}
