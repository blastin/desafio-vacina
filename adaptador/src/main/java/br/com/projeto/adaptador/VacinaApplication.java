package br.com.projeto.adaptador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VacinaApplication {

    public static void main(final String[] args) {
        SpringApplication.run(VacinaApplication.class, args);
    }

}
