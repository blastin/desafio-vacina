package br.com.projeto.adaptador.vacina;

import org.springframework.data.jpa.repository.JpaRepository;

interface VacinaRepositorio extends JpaRepository<VacinalEntity, Long> {
}
