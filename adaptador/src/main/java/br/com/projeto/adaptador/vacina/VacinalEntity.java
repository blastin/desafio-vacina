package br.com.projeto.adaptador.vacina;

import br.com.projeto.dominio.vacina.VacinaElement;

import javax.persistence.*;
import java.time.LocalDate;

@SuppressWarnings("unused")
@Entity
@Table(name = "vacina")
final class VacinalEntity {

    protected VacinalEntity() {
    }

    VacinalEntity(final VacinaElement vacina) {
        nome = vacina.nome();
        email = vacina.email();
        dataRealizacao = vacina.dataRealizacao();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vacina_sequence")
    @SequenceGenerator(name = "vacina_sequence", sequenceName = "vacina_seq")
    private long id;

    private String nome;

    private String email;

    private LocalDate dataRealizacao;

    long salvar(final VacinaRepositorio vacinaRepositorio) {
        return vacinaRepositorio.save(this).id;
    }

}
