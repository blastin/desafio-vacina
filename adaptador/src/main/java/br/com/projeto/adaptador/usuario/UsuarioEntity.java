package br.com.projeto.adaptador.usuario;

import br.com.projeto.dominio.usuario.Usuario;
import br.com.projeto.dominio.usuario.UsuarioElement;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "usuario")
public final class UsuarioEntity {

    protected UsuarioEntity() {
    }

    public UsuarioEntity(final UsuarioElement usuario) {
        nome = usuario.nome();
        email = usuario.email();
        cpf = usuario.cpf();
        dataNascimento = usuario.dataNascimento();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_sequence")
    @SequenceGenerator(name = "usuario_sequence", sequenceName = "usuario_seq")
    @SuppressWarnings("unused")
    private Long id;

    private String nome;

    private String email;

    private String cpf;

    private LocalDate dataNascimento;

    private Usuario toUsuario() {
        return
                Usuario
                        .builder()
                        .id(id)
                        .comNome(nome)
                        .paraEmail(email)
                        .cpf(cpf)
                        .nasceuEm(dataNascimento)
                        .build();
    }

    Usuario salvar(final UsuarioRepositorio usuarioRepositorio) {
        return usuarioRepositorio.save(this).toUsuario();
    }
}
