package br.com.projeto.adaptador.usuario;

import br.com.projeto.aplicacao.usuario.CasoDeUsoCadastrarUsuario;
import br.com.projeto.aplicacao.usuario.UsuarioRequest;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@SuppressWarnings("unused")
final class UsuarioRequisicao implements UsuarioRequest {

    @NotBlank
    private String nome;

    public void setNome(final String nome) {
        this.nome = nome;
    }

    @Override
    public String nome() {
        return nome;
    }

    @Email
    private String email;

    public void setEmail(final String email) {
        this.email = email;
    }

    @Override
    public String email() {
        return email;
    }

    @CPF
    private String cpf;

    public void setCpf(final String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String cpf() {
        return cpf;
    }

    @NotNull
    private LocalDate dataNascimento;

    public void setDataNascimento(final LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public LocalDate dataNascimento() {
        return dataNascimento;
    }

    UsuarioResposta cadastrar(final CasoDeUsoCadastrarUsuario casoDeUsoCadastrarUsuario) {
        return new UsuarioResposta(casoDeUsoCadastrarUsuario.cadastrar(this));
    }

}
