package br.com.projeto.adaptador.usuario;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

final class UsuarioResposta {

    UsuarioResposta(final Long identificador) {
        this.identificador = identificador;
    }

    private final Long identificador;

    @SuppressWarnings("unused")
    public long getId() {
        return identificador;
    }

    @JsonIgnore
    ResponseEntity<UsuarioResposta> toResponse() {
        return ResponseEntity.status(HttpStatus.CREATED).body(this);
    }

}
