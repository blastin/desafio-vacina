package br.com.projeto.adaptador.usuario;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepositorio extends JpaRepository<UsuarioEntity, Long> {

    boolean existsByCpfOrEmail(final String cpf, final String email);

    boolean existsByEmail(final String email);

}
