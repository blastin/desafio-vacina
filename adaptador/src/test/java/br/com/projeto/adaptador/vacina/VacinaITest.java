package br.com.projeto.adaptador.vacina;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class VacinaITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void quandoEmailNaoEncontradoDeveRetornarUsuarioNaoEncontrado() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/vacina")
                                        .content(Files
                                                .readAllBytes
                                                        (
                                                                ResourceUtils
                                                                        .getFile("classpath:br/com/projeto/adaptador/vacina/vacina-com-email-nao-encontrado-requisicao.json")
                                                                        .toPath()
                                                        )
                                        )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json
                        (Files
                                .readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/vacina/vacina-com-email-nao-encontrado-resposta.json")
                                                        .toPath()
                                        )
                        )
                );
    }

    @Test
    @Sql("usuario-para-vacina.sql")
    void quandoVacinaCadastradaDeveRetornarId() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/vacina")
                                        .content(Files
                                                .readAllBytes
                                                        (
                                                                ResourceUtils
                                                                        .getFile("classpath:br/com/projeto/adaptador/vacina/vacina-requisicao.json")
                                                                        .toPath()
                                                        )
                                        )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json
                        (Files
                                .readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/vacina/vacina-resposta.json")
                                                        .toPath()
                                        )
                        )
                );
    }

}

