package br.com.projeto.adaptador.usuario;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UsuarioInvalidacaoITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void quandoCpfDataNascimentoInvalidosDeveRetornarBadRequest() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/usuario")
                                        .content
                                                (Files
                                                        .readAllBytes
                                                                (
                                                                        ResourceUtils
                                                                                .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-cpf-datanascimento-invalidos-requisicao.json")
                                                                                .toPath()
                                                                )

                                                )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json
                        (Files
                                .readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-cpf-datanascimento-invalidos-resposta.json")
                                                        .toPath()
                                        )

                        )
                );

    }

    @Test
    void quandoEmailENomeInvalidosDeveRetornarBadRequest() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/usuario")
                                        .content
                                                (Files
                                                        .readAllBytes
                                                                (
                                                                        ResourceUtils
                                                                                .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-email-nome-invalidos-requisicao.json")
                                                                                .toPath()
                                                                )

                                                )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json
                        (Files
                                .readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-email-nome-invalidos-resposta.json")
                                                        .toPath()
                                        )

                        )
                );

    }
}
