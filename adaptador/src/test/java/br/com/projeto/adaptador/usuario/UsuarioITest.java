package br.com.projeto.adaptador.usuario;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UsuarioITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql("usuario-cpf-cadastrado.sql")
    void quandoUsuarioComCpfJahCadastradoDeveRetornarConflito() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/usuario")
                                        .content(Files
                                                .readAllBytes
                                                        (
                                                                ResourceUtils
                                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-com-cpf-duplicado-requisicao.json")
                                                                        .toPath()
                                                        )
                                        )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-com-cpf-duplicado-resposta.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @Sql("usuario-email-cadastrado.sql")
    void quandoUsuarioComEmailJahCadastradoDeveRetornarConflito() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/usuario")
                                        .content(Files
                                                .readAllBytes
                                                        (
                                                                ResourceUtils
                                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-com-email-duplicado-requisicao.json")
                                                                        .toPath()
                                                        )
                                        )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().json(Files
                                .readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-com-email-duplicado-resposta.json")
                                                        .toPath()
                                        )
                        )
                );

    }

    @Test
    void quandoUsuarioCadastradoDeveRetornarId() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/usuario")
                                        .content(Files
                                                .readAllBytes
                                                        (
                                                                ResourceUtils
                                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-requisicao.json")
                                                                        .toPath()
                                                        )
                                        )
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json
                        (
                                Files.readString
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:br/com/projeto/adaptador/usuario/usuario-resposta.json")
                                                        .toPath()
                                        )
                        )
                );

    }

}
