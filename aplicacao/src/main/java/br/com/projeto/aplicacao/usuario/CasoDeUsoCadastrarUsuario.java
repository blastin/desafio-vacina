package br.com.projeto.aplicacao.usuario;

public interface CasoDeUsoCadastrarUsuario {

    Long cadastrar(final UsuarioRequest usuarioRequest);

}
