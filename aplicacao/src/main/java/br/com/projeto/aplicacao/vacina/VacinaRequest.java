package br.com.projeto.aplicacao.vacina;

import br.com.projeto.dominio.usuario.UsuarioQuery;
import br.com.projeto.dominio.vacina.Vacina;
import br.com.projeto.dominio.vacina.VacinaCommand;

import java.time.LocalDate;

public interface VacinaRequest {

    String nome();

    String email();

    LocalDate dataRealizacao();

    default long cadastrar(final UsuarioQuery usuarioQuery, final VacinaCommand vacinaCommand) {
        return
                new Vacina(nome(), email(), dataRealizacao())
                        .cadastrar(vacinaCommand, usuarioQuery);
    }

}
