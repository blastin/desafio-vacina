package br.com.projeto.aplicacao.usuario;

import br.com.projeto.dominio.usuario.Usuario;
import br.com.projeto.dominio.usuario.UsuarioGateway;

import java.time.LocalDate;

public interface UsuarioRequest {

    String nome();

    String email();

    String cpf();

    LocalDate dataNascimento();

    default long cadastrar(final UsuarioGateway usuarioGateway) {
        return
                Usuario
                        .builder()
                        .nasceuEm(dataNascimento())
                        .cpf(cpf())
                        .paraEmail(email())
                        .comNome(nome())
                        .build()
                        .cadastrar(usuarioGateway);
    }
}
