package br.com.projeto.aplicacao.vacina;

public interface CasoDeUsoCadastrarVacina {

    long cadastrar(final VacinaRequest vacinaRequest);

}
