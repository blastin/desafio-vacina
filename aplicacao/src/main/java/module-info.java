module br.com.projeto.aplicacao {
    requires transitive br.com.projeto.dominio;
    exports br.com.projeto.aplicacao.usuario;
    exports br.com.projeto.aplicacao.vacina;
    opens br.com.projeto.aplicacao.usuario;
    opens br.com.projeto.aplicacao.vacina;
}