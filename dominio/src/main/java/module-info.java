module br.com.projeto.dominio {
    exports br.com.projeto.dominio;
    exports br.com.projeto.dominio.usuario;
    exports br.com.projeto.dominio.vacina;
    opens br.com.projeto.dominio.usuario;
    opens br.com.projeto.dominio.vacina;
    opens br.com.projeto.dominio;
}