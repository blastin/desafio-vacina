package br.com.projeto.dominio.vacina;

import java.time.LocalDate;

public final class VacinaMock extends Vacina {
    public VacinaMock() {
        super("nome", "email@email.com", LocalDate.now());
    }
}
