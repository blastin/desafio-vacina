package br.com.projeto.dominio.usuario;

import java.time.LocalDate;

public class Usuario {

    public static class UsuarioBuilder {

        private UsuarioBuilder() {
        }

        private UsuarioBuilder(final UsuarioElement usuario) {
            id = usuario.id();
            nome = usuario.nome();
            email = usuario.email();
            cpf = usuario.cpf();
            dataNascimento = usuario.dataNascimento();
        }

        private long id;

        public UsuarioBuilder id(final long id) {
            this.id = id;
            return this;
        }

        private String nome;

        public UsuarioBuilder comNome(final String nome) {
            this.nome = nome;
            return this;
        }

        private String email;

        public UsuarioBuilder paraEmail(final String email) {
            this.email = email;
            return this;
        }

        private String cpf;

        public UsuarioBuilder cpf(final String cpf) {
            this.cpf = cpf;
            return this;
        }

        private LocalDate dataNascimento;

        public UsuarioBuilder nasceuEm(final LocalDate dataNascimento) {
            this.dataNascimento = dataNascimento;
            return this;
        }

        public Usuario build() {
            return new Usuario(this);
        }

        private UsuarioElement toElement() {
            return new UsuarioElement() {
                @Override
                public long id() {
                    return id;
                }

                @Override
                public String nome() {
                    return nome;
                }

                @Override
                public String email() {
                    return email;
                }

                @Override
                public String cpf() {
                    return cpf;
                }

                @Override
                public LocalDate dataNascimento() {
                    return dataNascimento;
                }
            };
        }
    }

    public static UsuarioBuilder builder() {
        return new UsuarioBuilder();
    }

    public static UsuarioBuilder builder(final Usuario usuario) {
        return new UsuarioBuilder(usuario.usuarioBuilder.toElement());
    }

    Usuario(final UsuarioBuilder usuarioBuilder) {
        this.usuarioBuilder = usuarioBuilder;
    }

    private final UsuarioBuilder usuarioBuilder;

    public UsuarioElement toElement() {
        return usuarioBuilder.toElement();
    }

    public long cadastrar(final UsuarioGateway usuarioGateway) {
        return usuarioGateway
                .existeUsuarioComEmailOuCpf(usuarioBuilder.email, usuarioBuilder.cpf)
                .ifExiste
                        (
                                () -> new UsuarioException
                                        (
                                                "usuário existente com cpf '%s' ou email '%s'",
                                                usuarioBuilder.cpf,
                                                usuarioBuilder.email
                                        )
                        )
                .casoContrario(() -> usuarioGateway.cadastrar(toElement()).usuarioBuilder.id);
    }

}
