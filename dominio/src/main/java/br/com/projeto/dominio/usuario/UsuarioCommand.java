package br.com.projeto.dominio.usuario;

@FunctionalInterface
public interface UsuarioCommand {

    Usuario cadastrar(final UsuarioElement usuario);

}
