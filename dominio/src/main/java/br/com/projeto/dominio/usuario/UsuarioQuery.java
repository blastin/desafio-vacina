package br.com.projeto.dominio.usuario;

import br.com.projeto.dominio.Conditional;

public interface UsuarioQuery {

    Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf);

    boolean existeUsuarioPorEmail(final String email);

}
