package br.com.projeto.dominio.vacina;

public final class VacinaException extends RuntimeException {

    public VacinaException(final String s, final Object valor) {
        super(String.format(s, valor));
    }

}
