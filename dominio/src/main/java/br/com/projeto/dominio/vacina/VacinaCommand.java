package br.com.projeto.dominio.vacina;

@FunctionalInterface
public interface VacinaCommand {

    long cadastrar(final VacinaElement vacina);

}
