package br.com.projeto.dominio;

import java.util.function.Supplier;

public interface Conditional<T, R> {

    Conditional<T, R> ifExiste(final Supplier<? extends RuntimeException> supplierException);

    R casoContrario(final Supplier<? extends R> supplier);

    static <T, R> Conditional<T, R> from(final boolean bool) {
        return new ConditionalBasico<>(bool);
    }

}
