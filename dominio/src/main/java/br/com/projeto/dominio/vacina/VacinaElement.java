package br.com.projeto.dominio.vacina;

import java.time.LocalDate;

public interface VacinaElement {

    String nome();

    String email();

    LocalDate dataRealizacao();

    long id();

}
