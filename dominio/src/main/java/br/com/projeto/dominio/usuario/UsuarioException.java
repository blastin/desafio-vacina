package br.com.projeto.dominio.usuario;

public final class UsuarioException extends RuntimeException {

    UsuarioException(final String s, final Object... valores) {
        super(String.format(s, valores));
    }

}
