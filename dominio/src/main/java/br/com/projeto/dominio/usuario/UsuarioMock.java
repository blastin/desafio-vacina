package br.com.projeto.dominio.usuario;

import java.time.LocalDate;

public final class UsuarioMock extends Usuario {

    public UsuarioMock() {
        super
                (
                        Usuario
                                .builder
                                        (
                                                Usuario
                                                        .builder()
                                                        .comNome("Nome")
                                                        .cpf("374.265.050-57")
                                                        .paraEmail("email@email.com")
                                                        .nasceuEm(LocalDate.now().minusYears(20))
                                                        .id(1L)
                                                        .build()
                                        )
                );
    }

}
