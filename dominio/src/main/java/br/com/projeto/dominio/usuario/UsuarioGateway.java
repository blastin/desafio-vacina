package br.com.projeto.dominio.usuario;

public interface UsuarioGateway extends UsuarioCommand, UsuarioQuery {
}
