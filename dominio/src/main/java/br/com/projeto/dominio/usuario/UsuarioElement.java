package br.com.projeto.dominio.usuario;

import java.time.LocalDate;

public interface UsuarioElement {

    long id();

    String nome();

    String email();

    String cpf();

    LocalDate dataNascimento();

}
