package br.com.projeto.dominio.vacina;

import br.com.projeto.dominio.usuario.UsuarioQuery;

import java.time.LocalDate;

public class Vacina {

    public Vacina(final String nome, final String email, final LocalDate dataRealizacao) {
        this.nome = nome;
        this.email = email;
        this.dataRealizacao = dataRealizacao;
        id = 0L;
    }

    private final long id;

    private final String nome;

    private final String email;

    private final LocalDate dataRealizacao;

    public VacinaElement toElement() {
        return new VacinaElement() {
            @Override
            public String nome() {
                return nome;
            }

            @Override
            public String email() {
                return email;
            }

            @Override
            public LocalDate dataRealizacao() {
                return dataRealizacao;
            }

            @Override
            public long id() {
                return id;
            }
        };
    }

    public long cadastrar(final VacinaCommand vacinaCommand, final UsuarioQuery usuarioQuery) {

        if (usuarioQuery.existeUsuarioPorEmail(email)) return vacinaCommand.cadastrar(toElement());

        throw new VacinaException("não foi encontrado usuário com email '%s'", email);

    }
}
