package br.com.projeto.dominio;

import java.util.function.Supplier;

final class ConditionalBasico<T, R> implements Conditional<T, R> {

    ConditionalBasico(final boolean bool) {
        this.bool = bool;
    }

    private final boolean bool;

    @Override
    public Conditional<T, R> ifExiste(final Supplier<? extends RuntimeException> supplierException) {
        if (bool) throw supplierException.get();
        return this;
    }

    @Override
    public R casoContrario(final Supplier<? extends R> supplier) {
        return supplier.get();
    }

}
