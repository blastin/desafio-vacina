package br.com.projeto.dominio.vacina;

import br.com.projeto.dominio.Conditional;
import br.com.projeto.dominio.usuario.Usuario;
import br.com.projeto.dominio.usuario.UsuarioElement;
import br.com.projeto.dominio.usuario.UsuarioGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VacinaTest {

    @Test
    void quandoVacinaCadastrada() {

        Assertions.assertDoesNotThrow(() ->
                new VacinaMock()
                        .cadastrar(vacina -> 1L, new UsuarioGateway() {
                            @Override
                            public Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf) {
                                throw new IllegalCallerException("não deveria chegar aqui");
                            }

                            @Override
                            public Usuario cadastrar(final UsuarioElement usuario) {
                                throw new IllegalCallerException("não deveria chegar aqui");
                            }

                            @Override
                            public boolean existeUsuarioPorEmail(final String email) {
                                return true;
                            }
                        })
        );

    }

    @Test
    void quandoNaoFoiEncontradoUsuarioComEmailDeveSubirExcecao() {

        Assertions.assertThrows
                (
                        VacinaException.class,
                        () ->
                                new VacinaMock()
                                        .cadastrar(vacina -> 2L, new UsuarioGateway() {
                                            @Override
                                            public Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf) {
                                                throw new IllegalCallerException("não deveria chegar aqui");

                                            }

                                            @Override
                                            public Usuario cadastrar(final UsuarioElement usuario) {
                                                throw new IllegalCallerException("não deveria chegar aqui");
                                            }

                                            @Override
                                            public boolean existeUsuarioPorEmail(final String email) {
                                                return false;
                                            }
                                        })
                );

    }

    @Test
    void quandoRetornarElementosDeveSerIgualMock() {
        final VacinaElement vacinaElement = new VacinaMock().toElement();
        Assertions.assertEquals("nome", vacinaElement.nome());
        Assertions.assertEquals("email@email.com", vacinaElement.email());
        Assertions.assertNotNull(vacinaElement.dataRealizacao());
        Assertions.assertEquals(0L, vacinaElement.id());
    }
}