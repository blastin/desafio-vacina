package br.com.projeto.dominio.usuario;

import br.com.projeto.dominio.Conditional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UsuarioTest {

    @Test
    void quandoUsuarioFoiCadastrado() {

        final UsuarioMock usuarioMock = new UsuarioMock();

        Assertions.assertDoesNotThrow
                (
                        () -> {
                            usuarioMock
                                    .cadastrar
                                            (
                                                    new UsuarioGateway() {
                                                        @Override
                                                        public Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf) {
                                                            return Conditional.from(false);
                                                        }

                                                        @Override
                                                        public Usuario cadastrar(final UsuarioElement usuario) {
                                                            return usuarioMock;
                                                        }

                                                        @Override
                                                        public boolean existeUsuarioPorEmail(final String email) {
                                                            throw new IllegalCallerException("não deveria chegar aqui");
                                                        }
                                                    }
                                            );
                        }
                );

    }

    @Test
    void quandoEmailOuCpfEncontradoUsuarioNaoCadastrado() {
        Assertions.assertThrows
                (
                        UsuarioException.class,
                        () -> new UsuarioMock()
                                .cadastrar(new UsuarioGateway() {
                                    @Override
                                    public Conditional<Usuario, Long> existeUsuarioComEmailOuCpf(final String email, final String cpf) {
                                        return Conditional.from(true);
                                    }

                                    @Override
                                    public Usuario cadastrar(final UsuarioElement usuario) {
                                        throw new IllegalCallerException("não deveria chegar aqui");
                                    }

                                    @Override
                                    public boolean existeUsuarioPorEmail(final String email) {
                                        throw new IllegalCallerException("não deveria chegar aqui");
                                    }
                                })
                );

    }

}
