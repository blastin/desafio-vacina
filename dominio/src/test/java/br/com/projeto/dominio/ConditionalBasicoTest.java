package br.com.projeto.dominio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

class ConditionalBasicoTest {

    @Test
    void quandoPredicadoVerdadeiroSobeExcecao() {

        Assertions.assertThrows
                (
                        IllegalCallerException.class,
                        () -> new ConditionalBasico<>(true)
                                .ifExiste(IllegalCallerException::new)
                                .casoContrario(() -> {
                                    throw new AssertionError();
                                })
                );
    }

    @Test
    void quandoPredicadoFalsoNaoSobeExcecao() {

        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);

        Assertions.assertDoesNotThrow
                (
                        () -> new ConditionalBasico<>(atomicBoolean.get())
                                .ifExiste(IllegalCallerException::new)
                                .casoContrario(() -> atomicBoolean.getAndSet(true))
                );

        Assertions.assertTrue(atomicBoolean.get());

    }
}