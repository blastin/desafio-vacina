package br.com.projeto.fitness.arquitetura;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.lang.syntax.elements.ClassesShouldConjunction;
import com.tngtech.archunit.library.Architectures;

@SuppressWarnings("unused")
@AnalyzeClasses(packages = "br.com.projeto")
class ArquiteturaTest {

    @ArchTest
    static final Architectures.LayeredArchitecture ARCHITECTURES =
            Architectures
                    .layeredArchitecture()
                    .layer("Dominio").definedBy("br.com.projeto.dominio..")
                    .layer("Aplicação").definedBy("br.com.projeto.aplicacao..")
                    .layer("Adaptador").definedBy("br.com.projeto.adaptador..")
                    .layer("Fitness").definedBy("br.com.projeto.fitness..")
                    .whereLayer("Fitness").mayNotBeAccessedByAnyLayer()
                    .whereLayer("Adaptador").mayNotBeAccessedByAnyLayer()
                    .whereLayer("Aplicação").mayOnlyBeAccessedByLayers("Adaptador")
                    .whereLayer("Dominio").mayOnlyBeAccessedByLayers("Adaptador", "Aplicação");

    @ArchTest
    static final ClassesShouldConjunction DOMINIO_RULES =
            ArchRuleDefinition.classes()
                    .that()
                    .resideInAPackage("br.com.projeto.dominio..")
                    .should()
                    .onlyDependOnClassesThat()
                    .resideInAnyPackage("java..", "br.com.projeto.dominio..");

    @ArchTest
    static final ClassesShouldConjunction APLICACAO_RULES =
            ArchRuleDefinition.classes()
                    .that()
                    .resideInAPackage("br.com.projeto.aplicacao..")
                    .should()
                    .onlyDependOnClassesThat()
                    .resideInAnyPackage("java..", "br.com.projeto.aplicacao..", "br.com.projeto.dominio..");

}
